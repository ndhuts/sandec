package com.dc024.sandecworkshop.activity;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dc024.sandecworkshop.BuildConfig;
import com.dc024.sandecworkshop.R;
import com.dc024.sandecworkshop.helper.Config;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText etUserId;
    private EditText etPassword;
    private LinearLayout divLogin;
    private TextView tvAppVersion;
    private ProgressBar pbLogin;
    private ProgressDialog mProgressDialog;

    private boolean mDebugMode = false;
    private boolean mBusy = false;

    private String mUserId = "";
    private String mUserName = "";
    private String mUserId_ = "";
    private String mUserPhone = "";
    private String mKodeCabang = "";
    private String mNamaCabang = "";
    private String mLoginToken = "";
    private String mAvatarPath = "";

    //volley
    RequestQueue mRequestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //getSupportActionBar().hide();

        setContentView(R.layout.activity_login);

        etUserId = (EditText) findViewById(R.id.etUserId);
        etUserId.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            private void doNothing() {

            }

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if(!TextUtils.isEmpty(etUserId.getText().toString()) && !TextUtils.isEmpty(etPassword.getText().toString())) {
                        login();
                    }
                    if(!TextUtils.isEmpty(etUserId.getText().toString()) && TextUtils.isEmpty(etPassword.getText().toString())) {
                        etPassword.requestFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(etPassword, InputMethodManager.SHOW_IMPLICIT);
                    }
                }
                return false;
            }
        });
        etPassword = (EditText) findViewById(R.id.etPassword);
        etPassword.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            private void doNothing() {

            }

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if(!TextUtils.isEmpty(etPassword.getText().toString()) && !TextUtils.isEmpty(etUserId.getText().toString())) {
                        login();
                    }
                    if(!TextUtils.isEmpty(etPassword.getText().toString()) && TextUtils.isEmpty(etUserId.getText().toString())) {
                        etUserId.requestFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(etUserId, InputMethodManager.SHOW_IMPLICIT);
                    }
                }
                return false;
            }
        });

        tvAppVersion = (TextView) findViewById(R.id.tvAppVersion);
        //tvAppVersion.setText("Version " + BuildConfig.VERSION_CODE);
        tvAppVersion.setText("Ver. " + BuildConfig.VERSION_NAME);

        pbLogin = (ProgressBar) findViewById(R.id.pbLogin);
        pbLogin.setVisibility(View.GONE);

        divLogin = (LinearLayout) findViewById(R.id.divLogin);
        divLogin.setOnClickListener(this);

        //volley
        mRequestQueue = Volley.newRequestQueue(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mRequestQueue != null) {
            mRequestQueue.cancelAll(new RequestQueue.RequestFilter() {
                private void doNothing() {

                }

                @Override
                public boolean apply(Request<?> request) {
                    // do I have to cancel this?
                    return true; // -> always yes
                }
            });
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mRequestQueue != null) {
            mRequestQueue.cancelAll(new RequestQueue.RequestFilter() {
                private void doNothing() {

                }

                @Override
                public boolean apply(Request<?> request) {
                    // do I have to cancel this?
                    return true; // -> always yes
                }
            });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mRequestQueue != null) {
            mRequestQueue.cancelAll(new RequestQueue.RequestFilter() {
                private void doNothing() {

                }

                @Override
                public boolean apply(Request<?> request) {
                    // do I have to cancel this?
                    return true; // -> always yes
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sp = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        mUserId = sp.getString(Config.LOGIN_ID_SHARED_PREF, "");
        mUserId_ = sp.getString(Config.LOGIN_ID__SHARED_PREF, "");
        mUserName = sp.getString(Config.LOGIN_NAME_SHARED_PREF, "");
        mKodeCabang = sp.getString(Config.LOGIN_EXTRA_01_SHARED_PREF, "");
        mNamaCabang = sp.getString(Config.LOGIN_EXTRA_02_SHARED_PREF, "");
        mUserPhone = sp.getString(Config.LOGIN_PHONE_SHARED_PREF, "");
        mLoginToken = sp.getString(Config.LOGIN_TOKEN_SHARED_PREF, "");
        mAvatarPath = sp.getString(Config.LOGIN_AVATAR_SHARED_PREF, "");

        if (!mUserId.equalsIgnoreCase("") && !mUserName.equalsIgnoreCase(""))   dispatchToActivity();
    }

    private void login() {
        if(mBusy) {
            String message = "";
            message = "Tunggu proses sebelumnya selesai";
            SpannableStringBuilder biggerText = new SpannableStringBuilder(message);
            biggerText.setSpan(new RelativeSizeSpan(1.1f), 0, message.length(), 0);
            Toast.makeText(LoginActivity.this,
                    biggerText,
                    Toast.LENGTH_LONG).show();
            /*
            Toast.makeText(LoginActivity.this,
                    "Tunggu proses sebelumnya selesai",
                    Toast.LENGTH_SHORT).show();
            */
            return;
        }

        if(TextUtils.isEmpty(etUserId.getText().toString().trim()) || etUserId.getText().toString().trim().equalsIgnoreCase("") || TextUtils.isEmpty(etPassword.getText().toString().trim()) || etPassword.getText().toString().trim().equalsIgnoreCase("")) {
            String message = "";
            message = "Harap isikan nomor ponsel/kode anggota dan password";
            SpannableStringBuilder biggerText = new SpannableStringBuilder(message);
            biggerText.setSpan(new RelativeSizeSpan(1.1f), 0, message.length(), 0);
            Toast.makeText(LoginActivity.this, biggerText, Toast.LENGTH_LONG).show();
            //Toast.makeText(MainActivity.this, "Harap isikan user name dan password", Toast.LENGTH_SHORT).show();
            mBusy = false;
            return;
        }

        mBusy = true;
        divLogin.setVisibility(View.GONE);
        pbLogin.setVisibility(View.VISIBLE);

        Response.Listener<String> responseListener = new Response.Listener<String>() {
            //pancingan
            private void doNothing() {

            }

            @Override
            public void onResponse(String response) {
                pbLogin.setVisibility(View.GONE);
                mBusy = false;
                try {
                    if(mDebugMode) Toast.makeText(LoginActivity.this, "Response : " + response, Toast.LENGTH_LONG).show();

                    JSONObject jObj = new JSONObject(response);
                    String loginStatus = jObj.optString(Config.RESPONSE_STATUS_FIELD);
                    String message = jObj.optString(Config.RESPONSE_MESSAGE_FIELD);

                    if (loginStatus.trim().equalsIgnoreCase(Config.RESPONSE_STATUS_VALUE_SUCCESS)) {
                        JSONObject jsonPayload = jObj.optJSONObject(Config.RESPONSE_PAYLOAD_FIELD);

                        mUserId = jsonPayload.optString("cif");
                        mUserId_ = jsonPayload.optString("cif_");
                        mUserName = jsonPayload.optString("nama");
                        mUserPhone = jsonPayload.optString("telepon");
                        mLoginToken = jsonPayload.optString("loginToken");
                        mKodeCabang = jsonPayload.optString("kodeCabang");
                        mNamaCabang = jsonPayload.optString("namaCabang");
                        mAvatarPath = jsonPayload.optString("profile_img_path");

                        //Creating a shared preference
                        SharedPreferences sharedPreferences = LoginActivity.this.getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);

                        //Creating editor to store values to shared preferences
                        SharedPreferences.Editor editor = sharedPreferences.edit();

                        //Adding values to editor
                        editor.putString(Config.LOGIN_ID_SHARED_PREF, mUserId);
                        editor.putString(Config.LOGIN_ID__SHARED_PREF, mUserId_);
                        editor.putString(Config.LOGIN_NAME_SHARED_PREF, mUserName);
                        editor.putString(Config.LOGIN_PHONE_SHARED_PREF, mUserPhone);
                        editor.putString(Config.LOGIN_TOKEN_SHARED_PREF, mLoginToken);
                        editor.putString(Config.LOGIN_EXTRA_01_SHARED_PREF, mKodeCabang);
                        editor.putString(Config.LOGIN_EXTRA_02_SHARED_PREF, mNamaCabang);
                        editor.putString(Config.LOGIN_AVATAR_SHARED_PREF, mAvatarPath);

                        //Saving values to editor
                        editor.commit();

                        SpannableStringBuilder biggerText = new SpannableStringBuilder(message);
                        biggerText.setSpan(new RelativeSizeSpan(1.0f), 0, message.length(), 0);
                        Toast.makeText(LoginActivity.this, biggerText, Toast.LENGTH_LONG).show();
                        //Toast.makeText(MainActivity.this, jObj.optString(Config.RESPONSE_MESSAGE_FIELD), Toast.LENGTH_SHORT).show();

                        dispatchToActivity();
                    }
                    else {
                        SpannableStringBuilder biggerText = new SpannableStringBuilder(message);
                        biggerText.setSpan(new RelativeSizeSpan(1.0f), 0, message.length(), 0);
                        Toast.makeText(LoginActivity.this, biggerText, Toast.LENGTH_LONG).show();
                        //Toast.makeText(MainActivity.this, jObj.getString(Config.RESPONSE_MESSAGE_FIELD), Toast.LENGTH_LONG).show();

                        pbLogin.setVisibility(View.GONE);
                        divLogin.setVisibility(View.VISIBLE);
                    }
                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    pbLogin.setVisibility(View.GONE);
                    divLogin.setVisibility(View.VISIBLE);
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            //pancingan
            private void doNothing() {

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                pbLogin.setVisibility(View.GONE);
                divLogin.setVisibility(View.VISIBLE);
                mBusy = false;
                //Toast.makeText(LoginActivity.this, "Volley error", Toast.LENGTH_SHORT).show();
                //Toast.makeText(getActivity(), Config.ERROR_NETWORK, Toast.LENGTH_SHORT).show();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    String message = "TimeoutError || NoConnectionError";
                    SpannableStringBuilder biggerText = new SpannableStringBuilder(message);
                    biggerText.setSpan(new RelativeSizeSpan(1.1f), 0, message.length(), 0);
                    Toast.makeText(LoginActivity.this, biggerText, Toast.LENGTH_LONG).show();
                    //Toast.makeText(getActivity(),"TimeoutError || NoConnectionError", Toast.LENGTH_LONG).show();
                }
                else if (error instanceof AuthFailureError) {
                    String message = "AuthFailureError";
                    SpannableStringBuilder biggerText = new SpannableStringBuilder(message);
                    biggerText.setSpan(new RelativeSizeSpan(1.1f), 0, message.length(), 0);
                    Toast.makeText(LoginActivity.this, biggerText, Toast.LENGTH_LONG).show();
                    //Toast.makeText(getActivity(),"AuthFailureError", Toast.LENGTH_LONG).show();
                }
                else if (error instanceof ServerError) {
                    String message = "ServerError";
                    SpannableStringBuilder biggerText = new SpannableStringBuilder(message);
                    biggerText.setSpan(new RelativeSizeSpan(1.1f), 0, message.length(), 0);
                    Toast.makeText(LoginActivity.this, biggerText, Toast.LENGTH_LONG).show();
                    //Toast.makeText(getActivity(),"ServerError", Toast.LENGTH_LONG).show();
                }
                else if (error instanceof NetworkError) {
                    String message = "NetworkError";
                    SpannableStringBuilder biggerText = new SpannableStringBuilder(message);
                    biggerText.setSpan(new RelativeSizeSpan(1.1f), 0, message.length(), 0);
                    Toast.makeText(LoginActivity.this, biggerText, Toast.LENGTH_LONG).show();
                    //Toast.makeText(getActivity(),"NetworkError", Toast.LENGTH_LONG).show();
                }
                else if (error instanceof ParseError) {
                    String message = "ParseError";
                    SpannableStringBuilder biggerText = new SpannableStringBuilder(message);
                    biggerText.setSpan(new RelativeSizeSpan(1.1f), 0, message.length(), 0);
                    Toast.makeText(LoginActivity.this, biggerText, Toast.LENGTH_LONG).show();
                    //Toast.makeText(getActivity(),"ParseError", Toast.LENGTH_LONG).show();
                }
            }
        };

        StringRequest volleyRequest = new StringRequest(
                Request.Method.POST,
                Config.BASE_URL_API_A,
                responseListener,
                errorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("act", "login");
                params.put("id", etUserId.getText().toString().trim());
                params.put("password", etPassword.getText().toString().trim());

                return params;
            }
        };

        volleyRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        if(mRequestQueue == null) mRequestQueue = Volley.newRequestQueue(LoginActivity.this);
        mRequestQueue.add(volleyRequest);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.divLogin:
                login();
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Keluar aplikasi ?");
        alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            private void doNothing() {

            }

            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            public void onClick(DialogInterface arg0, int arg1) {
                finishAffinity();
            }
        });
        alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });

        //Showing the alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void dispatchToActivity() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }
}
