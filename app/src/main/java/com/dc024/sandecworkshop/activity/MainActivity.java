package com.dc024.sandecworkshop.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.dc024.sandecworkshop.R;
import com.dc024.sandecworkshop.fragment.Beranda01Fragment;
import com.dc024.sandecworkshop.fragment.Beranda02Fragment;
import com.dc024.sandecworkshop.fragment.InflaterAndSpinnerFragment;
import com.dc024.sandecworkshop.helper.Config;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private String mUserId = "";
    private String mUserName = "";
    private String mUserId_ = "";
    private String mUserPhone = "";
    private String mKodeCabang = "";
    private String mNamaCabang = "";
    private String mLoginToken = "";
    private String mUserAvatar = "";

    private DrawerLayout mDrawerLayout;
    private boolean mDebugMode = false;
    private boolean mBusy = false;

    private Fragment mNewFragment = null;
    private android.support.v4.app.FragmentTransaction mFragmentTransaction;

    private ImageView ivMnBeranda01, ivMnBeranda02, ivMnRV, ivMnInflater, ivMnSpinner, ivMnLogout;
    private TextView tvMnBeranda01, tvMnBeranda02, tvMnRV, tvMnInflater, tvMnSpinner, tvMnLogout;
    private LinearLayout divMnBeranda01, divMnBeranda02;

    //volley
    RequestQueue mRequestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //-- full width nav drawer
        //DisplayMetrics metrics = new DisplayMetrics();
        //getWindowManager().getDefaultDisplay().getMetrics(metrics);
        //DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) navigationView.getLayoutParams();
        //params.width = metrics.widthPixels;
        //navigationView.setLayoutParams(params);

        divMnBeranda01 = (LinearLayout) navigationView.findViewById(R.id.divMnBeranda01);
        divMnBeranda01.setOnClickListener(new MenuBeranda01Listener());
        ivMnBeranda01 = (ImageView) navigationView.findViewById(R.id.ivMnBeranda01);
        tvMnBeranda01 = (TextView) navigationView.findViewById(R.id.tvMnBeranda01);

        divMnBeranda02 = (LinearLayout) navigationView.findViewById(R.id.divMnBeranda02);
        divMnBeranda02.setOnClickListener(new MenuBeranda02Listener());
        ivMnBeranda02 = (ImageView) navigationView.findViewById(R.id.ivMnBeranda02);
        tvMnBeranda02 = (TextView) navigationView.findViewById(R.id.tvMnBeranda02);

        ivMnRV = (ImageView) navigationView.findViewById(R.id.ivMnRV);
        tvMnRV = (TextView) navigationView.findViewById(R.id.tvMnRV);

        ivMnInflater = (ImageView) navigationView.findViewById(R.id.ivMnInflater);
        tvMnInflater = (TextView) navigationView.findViewById(R.id.tvMnInflater);

        ivMnSpinner = (ImageView) navigationView.findViewById(R.id.ivMnSpinner);
        tvMnSpinner = (TextView) navigationView.findViewById(R.id.tvMnSpinner);

        ivMnLogout = (ImageView) navigationView.findViewById(R.id.ivMnLogout);
        tvMnLogout = (TextView) navigationView.findViewById(R.id.tvMnLogout);

        //default fragment
        goToFragment(Config.MENU_BERANDA_01, null);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Toast.makeText(MainActivity.this, "onNavigationItemSelected\nID : " + id, Toast.LENGTH_SHORT).show();

//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        boolean askExitApp = false;

        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
        else {
            int count = getSupportFragmentManager().getBackStackEntryCount();
            //Toast.makeText(MainActivity.this, "count : " + count, Toast.LENGTH_SHORT).show();
            android.support.v4.app.Fragment f = getSupportFragmentManager().findFragmentById(R.id.frameContainer);

            if (count == 1 || f instanceof Beranda01Fragment || f instanceof Beranda02Fragment) {
                if(mUserId.equalsIgnoreCase("")) {
                    askExitApp = true;
                }
                else {
                    moveTaskToBack(true);
                }
            }
            else {
                //if(f instanceof RecipeDetailFragment || f instanceof FoodBibleDetailFragment || f instanceof RecommendationDetailFragment) {
                //    getSupportFragmentManager().popBackStack();
                //}
                //else {
                //    FragmentToVisit.setFragmentName(Config.MENU_HOME);
                //    goToFragment(Config.MENU_HOME, null);
                //    //moveTaskToBack(true);
                //}
            }

            if(askExitApp) {
                //super.onBackPressed();
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("Keluar aplikasi ?");
                alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    private void doNothing() {

                    }

                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface arg0, int arg1) {
                        finishAffinity();
                    }
                });
                alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    private void doNothing() {

                    }

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();
                    }
                });

                //Showing the alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        }
    }

    public void setActiveMenu(String menu) {
        ivMnBeranda01.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorTextMenu), PorterDuff.Mode.SRC_ATOP);
        tvMnBeranda01.setTextColor(getResources().getColor(R.color.colorTextMenu));

        ivMnBeranda02.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorTextMenu), PorterDuff.Mode.SRC_ATOP);
        tvMnBeranda02.setTextColor(getResources().getColor(R.color.colorTextMenu));

        ivMnRV.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorTextMenu), PorterDuff.Mode.SRC_ATOP);
        tvMnRV.setTextColor(getResources().getColor(R.color.colorTextMenu));

        ivMnInflater.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorTextMenu), PorterDuff.Mode.SRC_ATOP);
        tvMnInflater.setTextColor(getResources().getColor(R.color.colorTextMenu));

        ivMnSpinner.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorTextMenu), PorterDuff.Mode.SRC_ATOP);
        tvMnSpinner.setTextColor(getResources().getColor(R.color.colorTextMenu));

        ivMnLogout.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorTextMenu), PorterDuff.Mode.SRC_ATOP);
        tvMnLogout.setTextColor(getResources().getColor(R.color.colorTextMenu));

        if(menu.equalsIgnoreCase(Config.MENU_BERANDA_01))   {
            tvMnBeranda01.setTextColor(getResources().getColor(R.color.colorAccent));
            ivMnBeranda01.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        }
        //tabungan
        if(menu.equalsIgnoreCase(Config.MENU_BERANDA_02))   {
            tvMnBeranda02.setTextColor(getResources().getColor(R.color.colorAccent));
            ivMnBeranda02.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        }
        if(menu.equalsIgnoreCase(Config.MENU_RV))   {
            tvMnRV.setTextColor(getResources().getColor(R.color.colorAccent));
            ivMnRV.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        }
        if(menu.equalsIgnoreCase(Config.MENU_INFLATER))   {
            tvMnInflater.setTextColor(getResources().getColor(R.color.colorAccent));
            tvMnInflater.setTextColor(getResources().getColor(R.color.colorAccent));
            ivMnInflater.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        }
        if(menu.equalsIgnoreCase(Config.MENU_SPINNER))   {
            tvMnSpinner.setTextColor(getResources().getColor(R.color.colorAccent));
            ivMnSpinner.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        }
    }

    public void goToFragment(String toFragment, Bundle bundle) {
        mNewFragment = null;
        mFragmentTransaction = getSupportFragmentManager().beginTransaction();

        if(toFragment.equalsIgnoreCase(Config.MENU_BERANDA_01)) {
            mNewFragment = new Beranda01Fragment();
        }

        if(toFragment.equalsIgnoreCase(Config.MENU_BERANDA_02)) {
            mNewFragment = new Beranda02Fragment();
        }

        if(toFragment.equalsIgnoreCase(Config.MENU_SPINNER)) {
            mNewFragment = new InflaterAndSpinnerFragment();
        }

        if(toFragment.equalsIgnoreCase(Config.MENU_INFLATER
        )) {
            mNewFragment = new InflaterAndSpinnerFragment();
        }

        if(mNewFragment == null) {
            Toast.makeText(MainActivity.this, "Fitur belum tersedia", Toast.LENGTH_SHORT).show();
            return;
        }

        if(bundle != null)  mNewFragment.setArguments(bundle);

        setActiveMenu(toFragment);

        mFragmentTransaction.replace(R.id.frameContainer, mNewFragment);
        mFragmentTransaction.addToBackStack(null);
        mFragmentTransaction.commit();

        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    class MenuBeranda01Listener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            goToFragment(Config.MENU_BERANDA_01, null);
        }
    }

    class MenuBeranda02Listener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            SharedPreferences sp = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
            String userId = sp.getString(Config.LOGIN_ID_SHARED_PREF,"");

            if(userId.equalsIgnoreCase("") || TextUtils.isEmpty(userId) || userId.equalsIgnoreCase("") ) {
                Toast.makeText(MainActivity.this, "Maaf Anda harus login dahulu", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
            else {
                goToFragment(Config.MENU_BERANDA_02, null);
            }
        }
    }
}
