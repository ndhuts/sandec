package com.dc024.sandecworkshop.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class CustomEditTextLightItalic extends android.support.v7.widget.AppCompatEditText {

    public CustomEditTextLightItalic(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomEditTextLightItalic(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomEditTextLightItalic(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/OpenSans-LightItalic.ttf");
            setTypeface(tf);
        }
    }

}