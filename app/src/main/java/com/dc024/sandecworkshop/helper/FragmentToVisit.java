package com.dc024.sandecworkshop.helper;

/**
 * Created by rr on 4/19/18.
 */

public final class FragmentToVisit {
    private static String fragmentName = "";

    public static void setFragmentName(String f) {
        fragmentName = f;
    }

    public static String getFragmentName() {
        return fragmentName;
    }
}
