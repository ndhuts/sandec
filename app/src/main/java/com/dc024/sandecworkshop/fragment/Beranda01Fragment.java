package com.dc024.sandecworkshop.fragment;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.dc024.sandecworkshop.R;
import com.dc024.sandecworkshop.activity.MainActivity;
import com.dc024.sandecworkshop.helper.Config;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by rr on 4/13/18.
 */

public class Beranda01Fragment extends android.support.v4.app.Fragment {
    private String mUserId = "";
    private String mLoginToken = "";

    private SliderLayout sliderInfo;
    private LinearLayout div01, div02, div03, div04, div05, div06, div07, div08, div09;

    private boolean mDebugMode = false;

    private RequestQueue mRequestQueue;

    private MainActivity mMainActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sp = getActivity().getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        mUserId = sp.getString(Config.LOGIN_ID_SHARED_PREF, "");
        mLoginToken = sp.getString(Config.LOGIN_TOKEN_SHARED_PREF, "");

        mMainActivity = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_beranda_01, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //hide keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        //volley
        mRequestQueue = Volley.newRequestQueue(getActivity());

        //slider
        sliderInfo = (SliderLayout) view.findViewById(R.id.sliderInfo);
        getSliders();
        //displayLocalSliders();

        div01 = (LinearLayout) view.findViewById(R.id.div01);
        div01.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View v) {
                //mekanisme cek status login sdh dihandle di method goToFragment
                ((MainActivity) getActivity()).goToFragment(Config.MENU_BERANDA_02, null);
            }
        });

        div02 = (LinearLayout) view.findViewById(R.id.div02);
        div02.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                //mekanisme cek status login sdh dihandle di method goToFragment
                ((MainActivity) getActivity()).goToFragment(Config.MENU_RV, null);
            }
        });

        div03 = (LinearLayout) view.findViewById(R.id.div03);
        div03.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).goToFragment(Config.MENU_INFLATER, null);
            }
        });

        div04 = (LinearLayout) view.findViewById(R.id.div04);
        div04.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).goToFragment(Config.MENU_SPINNER, null);
            }
        });

        div05 = (LinearLayout) view.findViewById(R.id.div05);
        div05.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                Toast.makeText(mMainActivity, "div05", Toast.LENGTH_SHORT).show();
            }
        });

        div06 = (LinearLayout) view.findViewById(R.id.div06);
        div06.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                Toast.makeText(mMainActivity, "div06", Toast.LENGTH_SHORT).show();
            }
        });

        div07 = (LinearLayout) view.findViewById(R.id.div07);
        div07.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                Toast.makeText(mMainActivity, "div07", Toast.LENGTH_SHORT).show();
            }
        });

        div08 = (LinearLayout) view.findViewById(R.id.div08);
        div08.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                Toast.makeText(mMainActivity, "div08", Toast.LENGTH_SHORT).show();
            }
        });

        div09 = (LinearLayout) view.findViewById(R.id.div09);
        div09.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                Toast.makeText(mMainActivity, "div09", Toast.LENGTH_SHORT).show();
            }
        });

        displayLocalSliders();
    }

    private void getSliders() {
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            private void doNothing() {

            }

            @Override
            public void onResponse(String response) {
                if(mDebugMode) Toast.makeText(getActivity(), "Response : " + response, Toast.LENGTH_LONG).show();

                if(!TextUtils.isEmpty(response)) {
                    //-- parse mResult
                    try {
                        JSONObject jObj = new JSONObject(response);
                        String resultStatus = jObj.getString(Config.RESPONSE_STATUS_FIELD);

                        if (resultStatus.trim().equalsIgnoreCase(Config.RESPONSE_STATUS_VALUE_SUCCESS)) {
                            String message = jObj.optString(Config.RESPONSE_MESSAGE_FIELD);
                            final HashMap<String,String> nameMaps = new HashMap<String, String>();
                            final HashMap<String, String> urlMaps = new HashMap<String, String>();

                            JSONArray jsonPayload = jObj.optJSONArray(Config.RESPONSE_PAYLOAD_FIELD);

                            for(int i=0; i<jsonPayload.length(); i++) {
                                JSONObject aItem = jsonPayload.optJSONObject(i);

                                nameMaps.put(aItem.optString("PROMO_TITLE"), Config.BASE_URL_UPLOAD_B + aItem.optString("PROMO_IMG_PATH"));
                                urlMaps.put(aItem.optString("PROMO_TITLE"), aItem.optString("PROMO_URL"));
                            }

                            for(final String name : nameMaps.keySet()){
                                TextSliderView textSliderView = new TextSliderView(getActivity());
                                // initialize a SliderLayout
                                textSliderView
                                        .description(name)
                                        .image(nameMaps.get(name))
                                        .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                            private void doNothing() {

                                            }

                                            @Override
                                            public void onSliderClick(BaseSliderView slider) {
                                                if(mDebugMode)  Toast.makeText(getActivity(), "image : " + nameMaps.get(name) + "\nlink : " + urlMaps.get(name), Toast.LENGTH_SHORT).show();
                                                if(!urlMaps.get(name).equalsIgnoreCase("")) {
                                                    try {
                                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlMaps.get(name))));

                                                    }
                                                    catch(ActivityNotFoundException e) {
                                                        Toast.makeText(getActivity(), "Tidak ada aplikasi terpasang pada perangkat Anda untuk membuka link promo.", Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }
                                        })
                                        .setScaleType(BaseSliderView.ScaleType.Fit);
                                //add your extra information
                                textSliderView.bundle(new Bundle());
                                textSliderView.getBundle().putString("extra",name);
                                sliderInfo.addSlider(textSliderView);
                            }
                            //sliderInfo.setPresetTransformer(SliderLayout.Transformer.Default);
                            sliderInfo.setPresetTransformer(SliderLayout.Transformer.Accordion);
                            sliderInfo.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            sliderInfo.setCustomAnimation(new DescriptionAnimation());
                            //sliderInfo.setCustomAnimation(new ChildAnimation());
                            sliderInfo.setDuration(4000);
                        }
                        else {
                            Toast.makeText(getActivity(), jObj.getString(Config.RESPONSE_MESSAGE_FIELD), Toast.LENGTH_LONG).show();
                            JSONObject jsonPayload = jObj.optJSONObject(Config.RESPONSE_PAYLOAD_FIELD);
                            if(jsonPayload != null) {
                                if (jsonPayload.optString(Config.RESPONSE_PAYLOAD_API_ACTION).equalsIgnoreCase(Config.RESPONSE_PAYLOAD_API_ACTION_LOGOUT)) {
                                    Config.forceLogout(getActivity());
                                }
                            }

                            displayLocalSliders();
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //--
                }
                else {
                    displayLocalSliders();
                }
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            private void doNothing() {

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), Config.ERROR_NETWORK, Toast.LENGTH_SHORT).show();
            }
        };

        StringRequest dataRequest = new StringRequest(Request.Method.GET, Config.BASE_URL_API_B + "slider", responseListener, errorListener) {};
        dataRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        if(mRequestQueue == null) mRequestQueue = Volley.newRequestQueue(mMainActivity);
        mRequestQueue.add(dataRequest);
    }

    private void displayLocalSliders() {
        // Load Image Dari res/drawable
        HashMap<String,Integer> fileMaps = new HashMap<String, Integer>();
        fileMaps.put("Product Showcase #1",R.drawable.rsz_banner_lh_01);
        fileMaps.put("Product Showcase #2",R.drawable.rsz_banner_lh_02);
        fileMaps.put("Product Showcase #3",R.drawable.rsz_banner_lh_03);
        fileMaps.put("Product Showcase #4",R.drawable.rsz_banner_lh_04);

        for(String name : fileMaps.keySet()){
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(fileMaps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra",name);
            sliderInfo.addSlider(textSliderView);
        }
        //sliderInfo.setPresetTransformer(SliderLayout.Transformer.Default);
        sliderInfo.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderInfo.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderInfo.setCustomAnimation(new DescriptionAnimation());
        //sliderInfo.setCustomAnimation(new ChildAnimation());
        sliderInfo.setDuration(4000);
    }
}
