package com.dc024.sandecworkshop.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;

import com.dc024.sandecworkshop.R;
import com.dc024.sandecworkshop.helper.Config;

/**
 * Created by WSeven7 on 1/6/2017.
 */

public class SplashActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);  //manggil view
        Thread timerThread = new Thread(){
            private void doNothing() {

            }

            public void run() {
                try {
                    sleep(1000);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                    //SharedPreferences sp = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                    //String userId = sp.getString(Config.LOGIN_ID_SHARED_PREF,"");

                    //if(userId.equalsIgnoreCase("") || TextUtils.isEmpty(userId) || userId.equalsIgnoreCase("") ) {
                    //    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    //    startActivity(intent);
                    //}
                    //else {
                        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(intent);
                    //}
                }
            }
        };
        timerThread.start();
    }

    @Override
    protected void onPause(){
        super.onPause();
        finish();
    }
}
