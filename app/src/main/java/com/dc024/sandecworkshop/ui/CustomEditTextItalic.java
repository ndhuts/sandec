package com.dc024.sandecworkshop.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class CustomEditTextItalic extends android.support.v7.widget.AppCompatEditText {

    public CustomEditTextItalic(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomEditTextItalic(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomEditTextItalic(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/OpenSans-Italic.ttf");
            setTypeface(tf);
        }
    }

}