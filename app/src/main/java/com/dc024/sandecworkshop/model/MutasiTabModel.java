package com.dc024.sandecworkshop.model;

/**
 * Created by rr on 1/12/19.
 */

public class MutasiTabModel {
    private int id;
    private String nomor;
    private String tanggal;
    private String tanggalDisplay;
    private String bukti;
    private String kode;
    private String uraian;
    private String dk;
    private String mutasiDisplay;
    private String validasi;
    private double mutasi;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getTanggalDisplay() {
        return tanggalDisplay;
    }

    public void setTanggalDisplay(String tanggalDisplay) {
        this.tanggalDisplay = tanggalDisplay;
    }

    public String getBukti() {
        return bukti;
    }

    public void setBukti(String bukti) {
        this.bukti = bukti;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getDk() {
        return dk;
    }

    public void setDk(String dk) {
        this.dk = dk;
    }

    public String getMutasiDisplay() {
        return mutasiDisplay;
    }

    public void setMutasiDisplay(String mutasiDisplay) {
        this.mutasiDisplay = mutasiDisplay;
    }

    public String getValidasi() {
        return validasi;
    }

    public void setValidasi(String validasi) {
        this.validasi = validasi;
    }

    public String getUraian() {
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }

    public double getMutasi() {
        return mutasi;
    }

    public void setMutasi(double mutasi) {
        this.mutasi = mutasi;
    }
}
