package com.dc024.sandecworkshop.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class CustomEditTextSemibold extends android.support.v7.widget.AppCompatEditText {

    public CustomEditTextSemibold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomEditTextSemibold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomEditTextSemibold(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/OpenSans-Semibold.ttf");
            setTypeface(tf);
        }
    }

}