package com.dc024.sandecworkshop.helper;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Spinner;
import android.widget.Toast;

import com.dc024.sandecworkshop.activity.LoginActivity;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by WSeven7 on 1/2/2017.
 */

public final class Config {
    public static final String BASE_URL_A = "http://149.28.145.136/abs";
    public static final String BASE_URL_API_A = BASE_URL_A + "/api/index.php";
    public static final String BASE_URL_UPLOAD_A = BASE_URL_A + "/uploads/";

    public static final String BASE_URL_B = "http://149.28.145.136/lighthouse";
    public static final String BASE_URL_API_B = BASE_URL_B + "/apiv1/";
    public static final String BASE_URL_UPLOAD_B = BASE_URL_B + "/uploads/";

    public static final String SHARED_PREF_NAME = "SANDEC Workshop";
    public static final String LOGIN_NAME_SHARED_PREF = "NAME";
    public static final String LOGIN_ID_SHARED_PREF = "ID";
    public static final String LOGIN_ID__SHARED_PREF = "ID_";
    public static final String LOGIN_TOKEN_SHARED_PREF = "TOKEN";
    public static final String LOGIN_EMAIL_SHARED_PREF = "EMAIL";
    public static final String LOGIN_PHONE_SHARED_PREF = "PHONE";
    public static final String LOGIN_GROUP_SHARED_PREF = "GROUP";
    public static final String LOGIN_GROUP_ID_SHARED_PREF = "GROUP_ID";
    public static final String LOGIN_STATUS_SHARED_PREF = "loggedin";
    public static final String LOGIN_AVATAR_SHARED_PREF = "AVATAR";
    public static final String LOGIN_EXTRA_01_SHARED_PREF = "EXTRA_01";
    public static final String LOGIN_EXTRA_02_SHARED_PREF = "EXTRA_02";
    public static final String LOGIN_EXTRA_03_SHARED_PREF = "EXTRA_03";
    public static final String LOGIN_EXTRA_04_SHARED_PREF = "EXTRA_04";
    public static final String LOGIN_EXTRA_05_SHARED_PREF = "EXTRA_05";
    public static final String LOGIN_WILL_ACTIVATE_RESUME_ACTIVITY_SHARED_PREF = "WILL_ACTIVATE_RESUME_ACTIVITY";
    public static final String LOGIN_RESUME_ACTIVITY_CALLBACK = "RESUME_ACTIVITY_CALLBACK";

    public static final String FCM_TOKEN_REGISTRATION_COMPLETE = "FCM_TOKEN_REGISTRATION_COMPLETE";
    public static final String PUSH_NOTIFICATION = "PUSH_NOTIFICATION";

    public static final String TASK_TAG_SYNC = "sync_data";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    
    public static final String SHARED_PREF_TAG_TOKEN = "SHARED_PREF_TAG_TOKEN";

    public static final String RESPONSE_STATUS_FIELD = "STATUS";
    public static final String RESPONSE_STATUS_VALUE_SUCCESS = "SUCCESS";
    public static final String RESPONSE_STATUS_VALUE_ERROR = "ERROR";
    public static final String RESPONSE_MESSAGE_FIELD = "MESSAGE";
    public static final String RESPONSE_PAYLOAD_FIELD = "PAYLOAD";

    public static final String ERROR_NETWORK = "Periksa kembali jaringan Anda";

    public static final int KEYWORD_SEARCH_MIN_LENGTH = 4;

    public static final String MENU_BERANDA_01 = "BERANDA_01";
    public static final String MENU_BERANDA_02 = "BERANDA_02";
    public static final String MENU_RV = "RV";
    public static final String MENU_INFLATER = "INFLATER";
    public static final String MENU_SPINNER = "SPINNER";

    public static final int PERMISSION_LOCATION = 1;
    public static final int PERMISSION_CALL = 2;
    public static final int PERMISSION_WRITE_EXST = 3;
    public static final int PERMISSION_READ_EXST = 4;
    public static final int PERMISSION_CAMERA = 5;
    public static final int PERMISSION_ACCOUNTS = 6;
    public static final int PERMISSION_GPS_SETTINGS = 7;
    public static final int PERMISSION_SEND_SMS = 8;

    //File request code
    public static final int PICK_FILE_REQUEST = 1;

    public static final String RESPONSE_PAYLOAD_API_ACTION = "API_ACTION";
    public static final String RESPONSE_PAYLOAD_API_ACTION_LOGOUT = "LOGOUT";

    public static final String EVENT_BUS_RELOAD = "EB_RELOAD";

    public static final String PARAMETER_RECOMMENDATION_URL = "PARAMETER_RECOMMENDATION_URL";

    public static boolean isNetworkAvailable(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        // if no network is available networkInfo will be null
        // otherwise check if we are connected
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public static void forceLogout(Context context) {
        //Getting out shared preferences
        SharedPreferences preferences = context.getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        //Getting editor
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Config.LOGIN_STATUS_SHARED_PREF, false);
        editor.putString(Config.LOGIN_ID_SHARED_PREF, "");
        editor.putString(Config.LOGIN_ID__SHARED_PREF, "");
        editor.putString(Config.LOGIN_NAME_SHARED_PREF, "");
        editor.putString(Config.LOGIN_GROUP_SHARED_PREF, "");
        editor.putString(Config.LOGIN_GROUP_ID_SHARED_PREF, "");
        editor.putString(Config.LOGIN_TOKEN_SHARED_PREF, "");
        editor.putString(Config.LOGIN_EMAIL_SHARED_PREF, "");
        editor.putString(Config.LOGIN_PHONE_SHARED_PREF, "");
        editor.putString(Config.LOGIN_AVATAR_SHARED_PREF, "");
        editor.putString(Config.LOGIN_EXTRA_01_SHARED_PREF, "");
        editor.putString(Config.LOGIN_EXTRA_02_SHARED_PREF, "");
        editor.putString(Config.LOGIN_EXTRA_03_SHARED_PREF, "");
        editor.putString(Config.LOGIN_EXTRA_04_SHARED_PREF, "");
        editor.putString(Config.LOGIN_EXTRA_05_SHARED_PREF, "");
        editor.putString(Config.LOGIN_WILL_ACTIVATE_RESUME_ACTIVITY_SHARED_PREF, "");

        //Saving the sharedpreferences
        editor.commit();

        Toast.makeText(context, "Anda telah logout dari aplikasi.\nUntuk mengakses beberapa fitur, Anda harus login terlebih dahulu", Toast.LENGTH_LONG).show();
        //Starting login activity
        Intent intent = new Intent(context.getApplicationContext(), LoginActivity.class);
        context.startActivity(intent);
        //CachedUser.setUserModel(null); //kalo pake UserModel dan CachedUser
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission(final Context context, final String permissionType) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>=android.os.Build.VERSION_CODES.M) {
            if(permissionType.equalsIgnoreCase(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                if (ContextCompat.checkSelfPermission(context, permissionType) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permissionType)) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                        alertBuilder.setCancelable(true);
                        alertBuilder.setTitle("Ijin Akses");
                        alertBuilder.setMessage("Aplikasi memerlukan ijin akses ruang penyimpanan");
                        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            private void doNothing() {

                            }

                            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions((Activity) context, new String[]{permissionType}, PERMISSION_READ_EXST);
                            }
                        });
                        AlertDialog alert = alertBuilder.create();
                        alert.show();

                        //Toast.makeText(context, "HIT #1", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{permissionType}, PERMISSION_READ_EXST);
                        //Toast.makeText(context, "HIT #2", Toast.LENGTH_SHORT).show();
                    }
                    return false;
                }
                else {
                    return true;
                }
            }
            else if(permissionType.equalsIgnoreCase(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                if (ContextCompat.checkSelfPermission(context, permissionType) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permissionType)) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                        alertBuilder.setCancelable(true);
                        alertBuilder.setTitle("Ijin Akses");
                        alertBuilder.setMessage("Aplikasi memerlukan ijin akses ruang penyimpanan");
                        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            private void doNothing() {

                            }

                            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions((Activity) context, new String[]{permissionType}, PERMISSION_WRITE_EXST);
                            }
                        });
                        AlertDialog alert = alertBuilder.create();
                        alert.show();
                    }
                    else {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{permissionType}, PERMISSION_WRITE_EXST);
                    }
                    return false;
                }
                else {
                    return true;
                }
            }
            else if(permissionType.equalsIgnoreCase(Manifest.permission.CAMERA)) {
                if (ContextCompat.checkSelfPermission(context, permissionType) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permissionType)) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                        alertBuilder.setCancelable(true);
                        alertBuilder.setTitle("Ijin Akses");
                        alertBuilder.setMessage("Aplikasi memerlukan ijin akses kamera");
                        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            private void doNothing() {

                            }

                            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions((Activity) context, new String[]{permissionType}, PERMISSION_CAMERA);
                            }
                        });
                        AlertDialog alert = alertBuilder.create();
                        alert.show();

                        //Toast.makeText(context, "HIT #1", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{permissionType}, PERMISSION_CAMERA);

                        //Toast.makeText(context, "HIT #2", Toast.LENGTH_SHORT).show();
                    }
                    return false;
                }
                else {
                    return true;
                }
            }
            else if(permissionType.equalsIgnoreCase(Manifest.permission.ACCESS_COARSE_LOCATION) || permissionType.equalsIgnoreCase(Manifest.permission.ACCESS_FINE_LOCATION)) {
                if (ContextCompat.checkSelfPermission(context, permissionType) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permissionType)) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                        alertBuilder.setCancelable(true);
                        alertBuilder.setTitle("Ijin Akses");
                        alertBuilder.setMessage("Aplikasi memerlukan ijin akses lokasi");
                        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            private void doNothing() {

                            }

                            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions((Activity) context, new String[]{permissionType}, PERMISSION_LOCATION);
                            }
                        });
                        AlertDialog alert = alertBuilder.create();
                        alert.show();
                    }
                    else {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{permissionType}, PERMISSION_LOCATION);
                    }
                    return false;
                }
                else {
                    return true;
                }
            }
            else if(permissionType.equalsIgnoreCase(Manifest.permission.CALL_PHONE)) {
                if (ContextCompat.checkSelfPermission(context, permissionType) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permissionType)) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                        alertBuilder.setCancelable(true);
                        alertBuilder.setTitle("Ijin Akses");
                        alertBuilder.setMessage("Aplikasi memerlukan ijin akses telepon");
                        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            private void doNothing() {

                            }

                            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions((Activity) context, new String[]{permissionType}, PERMISSION_CALL);
                            }
                        });
                        AlertDialog alert = alertBuilder.create();
                        alert.show();
                    }
                    else {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{permissionType}, PERMISSION_CALL);
                    }
                    return false;
                }
                else {
                    return true;
                }
            }
            else if(permissionType.equalsIgnoreCase(Manifest.permission.SEND_SMS)) {
                if (ContextCompat.checkSelfPermission(context, permissionType) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permissionType)) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                        alertBuilder.setCancelable(true);
                        alertBuilder.setTitle("Ijin Akses");
                        alertBuilder.setMessage("Aplikasi memerlukan ijin akses mengirim SMS");
                        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            private void doNothing() {

                            }

                            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions((Activity) context, new String[]{permissionType}, PERMISSION_SEND_SMS);
                            }
                        });
                        AlertDialog alert = alertBuilder.create();
                        alert.show();
                    }
                    else {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{permissionType}, PERMISSION_SEND_SMS);
                    }
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                return false;
            }
        }
        else {
            return true;
        }
    }

    public static String getRegisterId(Context ctx) {
        //SharedPreferences sharedPreferences = ctx.getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        //String regId = sharedPreferences.getString(Config.SHARED_PREF_REGISTER_ID, "");

        //getting unique id for device
        String regId = Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);

        return regId;
    }

    //helper full size image from camera
    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) {
        // BEST QUALITY MATCH
        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float)height / (float)reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float)width / (float)reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    public static String getMetadata(Context context, String name) {
        try {
            ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            if (appInfo.metaData != null) {
                return appInfo.metaData.getString(name);
            }
        }
        catch (PackageManager.NameNotFoundException e) {
            return null;
        }

        return null;
    }

    public static boolean isGPSAvailable(Context ctx){
        LocationManager lm = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);

        return lm != null && (lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
    }

    public static boolean checkEmailFormat(String email) {
        if (!email.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
            return false;
        }
        else {
            return true;
        }
    }

    public static String randomString(int len) {
        final String DATA = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        Random RANDOM = new Random();

        StringBuilder sb = new StringBuilder(len);

        for (int i = 0; i < len; i++) {
            sb.append(DATA.charAt(RANDOM.nextInt(DATA.length())));
        }

        return sb.toString();
    }

    public static String formatYMD(int year, int month, int date) {
        String formattedDate = "";

        formattedDate += Integer.toString(year);
        formattedDate += "-";

        if(month < 10) {
            formattedDate += "0" + Integer.toString(month);
        }
        else {
            formattedDate += Integer.toString(month);
        }
        formattedDate += "-";

        if(date < 10)   {
            formattedDate += "0" + Integer.toString(date);
        }
        else {
            formattedDate += Integer.toString(date);
        }

        return formattedDate;
    }

    public static String formatDMY(int year, int month, int date) {
        String formattedDate = "";

        if(date < 10)   {
            formattedDate += "0" + Integer.toString(date);
        }
        else {
            formattedDate += Integer.toString(date);
        }
        formattedDate += "-";

        if(month < 10) {
            formattedDate += "0" + Integer.toString(month);
        }
        else {
            formattedDate += Integer.toString(month);
        }
        formattedDate += "-";

        formattedDate += Integer.toString(year);

        return formattedDate;
    }

    public static String formatCustomTime(int hour, int minute) {
        String formattedTime = "";

        if(hour < 10) {
            formattedTime = "0" + Integer.toString(hour);
        }
        else {
            formattedTime = Integer.toString(hour);
        }

        formattedTime += ":";

        if(minute < 10) {
            formattedTime += "0" + Integer.toString(minute);
        }
        else {
            formattedTime += Integer.toString(minute);
        }

        return formattedTime;
    }

    public static String formatCustomDate(int year, int month, int date) {
        String formattedDate = "";

        if(date < 10)   {
            formattedDate = "0" + Integer.toString(date);
        }
        else {
            formattedDate = Integer.toString(date);
        }

        if(month == 1)  formattedDate += " Jan ";
        if(month == 2)  formattedDate += " Feb ";
        if(month == 3)  formattedDate += " Mar ";
        if(month == 4)  formattedDate += " Apr ";
        if(month == 5)  formattedDate += " Mei ";
        if(month == 6)  formattedDate += " Jun ";
        if(month == 7)  formattedDate += " Jul ";
        if(month == 8)  formattedDate += " Ags ";
        if(month == 9)  formattedDate += " Sep ";
        if(month == 10)  formattedDate += " Okt ";
        if(month == 11)  formattedDate += " Nov ";
        if(month == 12)  formattedDate += " Des ";

        return (formattedDate + Integer.toString(year));
    }

    public static int getIndex(ArrayList<ItemOption> list, Spinner spinner, String searchId){
        for (int i=0;i<spinner.getCount();i++){
            if (list.get(i).getOptId().equalsIgnoreCase(searchId)){
                return i;
            }
        }

        return 0;
    }
}
