package com.dc024.sandecworkshop.fragment;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.dc024.sandecworkshop.R;
import com.dc024.sandecworkshop.activity.MainActivity;
import com.dc024.sandecworkshop.helper.Config;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by rr on 1/11/19.
 */

public class Beranda02Fragment extends Fragment {
    private String mUserId = "";
    private String mUserName = "";
    private String mUserId_ = "";
    private String mUserPhone = "";
    private String mKodeCabang = "";
    private String mNamaCabang = "";
    private String mLoginToken = "";

    private SliderLayout sliderInfo;
    //private LinearLayout divTabSaldo, divTabMutasi, divTabNotifikasi, divTabTransfer, divTabRiwayatTransfer, divDepSaldo, divDepNotifikasi, divPinjSaldo, divPinjTagihan, divPinjRiwayat, divPinjNotifikasi, divDonasi;
    private CardView cvTabSaldo, cvTabMutasi, cvTabNotifikasi, cvTabTransfer, cvTabRiwayatTransfer, cvDepSaldo, cvDepNotifikasi, cvPinjSaldo, cvPinjTagihan, cvPinjRiwayat, cvPinjNotifikasi, cvDonasi, cvMPPulsa, cvMPPaketData, cvMPPascaBayar, cvMPEMoney, cMPAsuransi, cvMPLeasing, cvMPPLN, cvMPTvKabel, cvMPPDAM;

    private boolean mDebugMode = false;

    private RequestQueue mRequestQueue;

    private MainActivity mMainActivity;

    public static Beranda02Fragment newInstance() {
        Bundle bundle = new Bundle();

        Beranda02Fragment aFragment = new Beranda02Fragment();
        aFragment.setArguments(bundle);

        return aFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sp = getActivity().getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        mLoginToken = sp.getString(Config.LOGIN_TOKEN_SHARED_PREF, "");
        mUserId = sp.getString(Config.LOGIN_ID_SHARED_PREF, "");
        mUserId_ = sp.getString(Config.LOGIN_ID__SHARED_PREF, "");
        mUserName = sp.getString(Config.LOGIN_NAME_SHARED_PREF, "");
        mUserPhone = sp.getString(Config.LOGIN_PHONE_SHARED_PREF, "");
        mKodeCabang = sp.getString(Config.LOGIN_EXTRA_01_SHARED_PREF, "");
        mNamaCabang = sp.getString(Config.LOGIN_EXTRA_02_SHARED_PREF, "");

        mMainActivity = (MainActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();

        if(mLoginToken.equalsIgnoreCase("")) {
            Toast.makeText(mMainActivity, "Maaf, sesi telah habis", Toast.LENGTH_SHORT).show();
            Config.forceLogout(mMainActivity);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_beranda_02, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //hide keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        //volley
        mRequestQueue = Volley.newRequestQueue(getActivity());

        //slider
        sliderInfo = (SliderLayout) view.findViewById(R.id.sliderInfo);
        getSliders();
        //displayLocalSliders();

        cvTabSaldo = (CardView) view.findViewById(R.id.cvTabSaldo);
        cvTabSaldo.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                //mMainActivity.goToFragment(Config.MENU_TAB_INFO, null);
            }
        });

        cvTabMutasi = (CardView) view.findViewById(R.id.cvTabMutasi);
        cvTabMutasi.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                //mMainActivity.goToFragment(Config.MENU_TAB_MUTASI, null);
            }
        });

        cvTabNotifikasi = (CardView) view.findViewById(R.id.cvTabNotifikasi);
        cvTabNotifikasi.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                //mMainActivity.goToFragment(Config.MENU_TAB_NOTIFIKASI, null);
            }
        });

        cvTabTransfer = (CardView) view.findViewById(R.id.cvTabTransfer);
        cvTabTransfer.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                //mMainActivity.goToFragment(Config.MENU_TAB_TRANSFER, null);
            }
        });

        cvTabRiwayatTransfer = (CardView) view.findViewById(R.id.cvTabRiwayatTransfer);
        cvTabRiwayatTransfer.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                //mMainActivity.goToFragment(Config.MENU_TAB_RIWAYAT_TRANSFER, null);
            }
        });

        cvDepSaldo = (CardView) view.findViewById(R.id.cvDepSaldo);
        cvDepSaldo.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                //mMainActivity.goToFragment(Config.MENU_DEP_INFO, null);
            }
        });

        cvDepNotifikasi = (CardView) view.findViewById(R.id.cvDepNotifikasi);
        cvDepNotifikasi.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                //mMainActivity.goToFragment(Config.MENU_DEP_NOTIFIKASI, null);
            }
        });

        cvPinjSaldo = (CardView) view.findViewById(R.id.cvPinjSaldo);
        cvPinjSaldo.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                //mMainActivity.goToFragment(Config.MENU_PIN_INFO, null);
            }
        });

        cvPinjTagihan = (CardView) view.findViewById(R.id.cvPinjTagihan);
        cvPinjTagihan.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                //mMainActivity.goToFragment(Config.MENU_PIN_TAGIHAN, null);
            }
        });

        //divPinjRiwayat = (LinearLayout) view.findViewById(R.id.divPinjRiwayat);
        //divPinjNotifikasi = (LinearLayout) view.findViewById(R.id.divPinjNotifikasi);

        cvDonasi = (CardView) view.findViewById(R.id.cvDonasi);
        cvDonasi.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                //mMainActivity.goToFragment(Config.MENU_DONASI, null);
            }
        });

        cvMPPulsa = (CardView) view.findViewById(R.id.cvMPPulsa);
        cvMPPulsa.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                //mMainActivity.goToFragment(Config.MENU_MP_PULSA_REGULER, null);
            }
        });

        cvMPPaketData = (CardView) view.findViewById(R.id.cvMPPaketData);
        cvMPPaketData.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                //mMainActivity.goToFragment(Config.MENU_MP_PAKET_DATA, null);
            }
        });
    }

    private void getSliders() {
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            private void doNothing() {

            }

            @Override
            public void onResponse(String response) {
                if(mDebugMode) Toast.makeText(getActivity(), "Response : " + response, Toast.LENGTH_LONG).show();

                if(!TextUtils.isEmpty(response)) {
                    //-- parse mResult
                    try {
                        JSONObject jObj = new JSONObject(response);
                        String resultStatus = jObj.getString(Config.RESPONSE_STATUS_FIELD);

                        if (resultStatus.trim().equalsIgnoreCase(Config.RESPONSE_STATUS_VALUE_SUCCESS)) {
                            String message = jObj.optString(Config.RESPONSE_MESSAGE_FIELD);
                            final HashMap<String,String> nameMaps = new HashMap<String, String>();
                            final HashMap<String, String> urlMaps = new HashMap<String, String>();

                            JSONArray jsonPayload = jObj.optJSONArray(Config.RESPONSE_PAYLOAD_FIELD);

                            for(int i=0; i<jsonPayload.length(); i++) {
                                JSONObject aItem = jsonPayload.optJSONObject(i);

                                nameMaps.put(aItem.optString("PROMO_TITLE"), Config.BASE_URL_UPLOAD_B + aItem.optString("PROMO_IMG_PATH"));
                                urlMaps.put(aItem.optString("PROMO_TITLE"), aItem.optString("PROMO_URL"));
                            }

                            for(final String name : nameMaps.keySet()){
                                TextSliderView textSliderView = new TextSliderView(getActivity());
                                // initialize a SliderLayout
                                textSliderView
                                        .description(name)
                                        .image(nameMaps.get(name))
                                        .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                            private void doNothing() {

                                            }

                                            @Override
                                            public void onSliderClick(BaseSliderView slider) {
                                                if(mDebugMode)  Toast.makeText(getActivity(), "image : " + nameMaps.get(name) + "\nlink : " + urlMaps.get(name), Toast.LENGTH_SHORT).show();
                                                if(!urlMaps.get(name).equalsIgnoreCase("")) {
                                                    try {
                                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlMaps.get(name))));

                                                    }
                                                    catch(ActivityNotFoundException e) {
                                                        Toast.makeText(getActivity(), "Tidak ada aplikasi terpasang pada perangkat Anda untuk membuka link promo.", Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }
                                        })
                                        .setScaleType(BaseSliderView.ScaleType.Fit);
                                //add your extra information
                                textSliderView.bundle(new Bundle());
                                textSliderView.getBundle().putString("extra",name);
                                sliderInfo.addSlider(textSliderView);
                            }
                            //sliderInfo.setPresetTransformer(SliderLayout.Transformer.Default);
                            sliderInfo.setPresetTransformer(SliderLayout.Transformer.Accordion);
                            sliderInfo.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            sliderInfo.setCustomAnimation(new DescriptionAnimation());
                            //sliderInfo.setCustomAnimation(new ChildAnimation());
                            sliderInfo.setDuration(4000);
                        }
                        else {
                            Toast.makeText(getActivity(), jObj.getString(Config.RESPONSE_MESSAGE_FIELD), Toast.LENGTH_LONG).show();
                            JSONObject jsonPayload = jObj.optJSONObject(Config.RESPONSE_PAYLOAD_FIELD);
                            if(jsonPayload != null) {
                                if (jsonPayload.optString(Config.RESPONSE_PAYLOAD_API_ACTION).equalsIgnoreCase(Config.RESPONSE_PAYLOAD_API_ACTION_LOGOUT)) {
                                    Config.forceLogout(getActivity());
                                }
                            }

                            displayLocalSliders();
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //--
                }
                else {
                    displayLocalSliders();
                }
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            private void doNothing() {

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), Config.ERROR_NETWORK, Toast.LENGTH_SHORT).show();
            }
        };

        StringRequest dataRequest = new StringRequest(Request.Method.GET, Config.BASE_URL_API_B + "slider", responseListener, errorListener) {};
        dataRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        if(mRequestQueue == null) mRequestQueue = Volley.newRequestQueue(mMainActivity);
        mRequestQueue.add(dataRequest);
    }

    private void displayLocalSliders() {
        sliderInfo.setVisibility(View.VISIBLE);

        // Load Image Dari res/drawable
        HashMap<String,Integer> fileMaps = new HashMap<String, Integer>();
        fileMaps.put("Slider #1",R.drawable.rsz_banner_lh_01);
        fileMaps.put("Slider #2",R.drawable.rsz_banner_lh_02);
        fileMaps.put("Slider #3",R.drawable.rsz_banner_lh_03);

        for(String name : fileMaps.keySet()){
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(fileMaps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra",name);
            sliderInfo.addSlider(textSliderView);
        }
        //sliderInfo.setPresetTransformer(SliderLayout.Transformer.Default);
        sliderInfo.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderInfo.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderInfo.setCustomAnimation(new DescriptionAnimation());
        //sliderInfo.setCustomAnimation(new ChildAnimation());
        sliderInfo.setDuration(4000);
    }
}
