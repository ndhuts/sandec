package com.dc024.sandecworkshop.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dc024.sandecworkshop.R;
import com.dc024.sandecworkshop.activity.MainActivity;
import com.dc024.sandecworkshop.helper.Config;
import com.dc024.sandecworkshop.helper.ItemOption;
import com.dc024.sandecworkshop.model.MutasiTabModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by rr on 1/11/19.
 */

public class InflaterAndSpinnerFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private String mUserId = "";
    private String mUserName = "";
    private String mUserId_ = "";
    private String mUserPhone = "";
    private String mKodeCabang = "";
    private String mNamaCabang = "";
    private String mLoginToken = "";

    private ArrayList<MutasiTabModel> mList = new ArrayList<MutasiTabModel>();

    private boolean mDebugMode = false;

    private RequestQueue mRequestQueue;

    private MainActivity mMainActivity;

    private Spinner spnRekening;
    private ArrayList<ItemOption> mRekeningOption = new ArrayList<ItemOption>();
    private ArrayList<String> mRekeningStringList = new ArrayList<String>();
    private String mSelectedRekening = "";

    private Spinner spnBln;
    private ArrayList<ItemOption> mBlnOption = new ArrayList<ItemOption>();
    private ArrayList<String> mBlnStringList = new ArrayList<String>();
    private String mSelectedBln = "";

    private TextView tvRKSaldoAwal, tvRKSaldoAkhir, tvRKMutasiDb, tvRKMutasiCr;
    private LinearLayout divRefresh, divMenu;
    private ProgressBar pb;
    private LinearLayout divContainerMutasi;
    private SwipeRefreshLayout swipeRefresh;

    public static InflaterAndSpinnerFragment newInstance() {
        Bundle bundle = new Bundle();

        InflaterAndSpinnerFragment aFragment = new InflaterAndSpinnerFragment();
        aFragment.setArguments(bundle);

        return aFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sp = getActivity().getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        mLoginToken = sp.getString(Config.LOGIN_TOKEN_SHARED_PREF, "");
        mUserId = sp.getString(Config.LOGIN_ID_SHARED_PREF, "");
        mUserId_ = sp.getString(Config.LOGIN_ID__SHARED_PREF, "");
        mUserName = sp.getString(Config.LOGIN_NAME_SHARED_PREF, "");
        mUserPhone = sp.getString(Config.LOGIN_PHONE_SHARED_PREF, "");
        mKodeCabang = sp.getString(Config.LOGIN_EXTRA_01_SHARED_PREF, "");
        mNamaCabang = sp.getString(Config.LOGIN_EXTRA_02_SHARED_PREF, "");

        mMainActivity = (MainActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();

        if(mLoginToken.equalsIgnoreCase("")) {
            Toast.makeText(mMainActivity, "Maaf, sesi telah habis", Toast.LENGTH_SHORT).show();
            Config.forceLogout(mMainActivity);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_inflater_spinner, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //hide keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        //volley
        mRequestQueue = Volley.newRequestQueue(getActivity());

        spnRekening = (Spinner) view.findViewById(R.id.spnRekening);

        final Calendar c = Calendar.getInstance();
        int bln  = c.get(Calendar.MONTH) + 1;
        int thn = c.get(Calendar.YEAR);
        if(bln < 10) {
            mSelectedBln = "0" + bln;
        }
        else {
            mSelectedBln = "" + bln;
        }
        if(mDebugMode)  Toast.makeText(getActivity(), "mSelectedBln : " + mSelectedBln, Toast.LENGTH_SHORT).show();

        spnBln = (Spinner) view.findViewById(R.id.spnBln);
        mBlnOption = new ArrayList<ItemOption>();
        mBlnStringList = new ArrayList<String>();

        ItemOption aItmOpt = new ItemOption("-12", "Desember " + String.valueOf(thn - 1));
        mBlnOption.add(aItmOpt);
        mBlnStringList.add(aItmOpt.getOptLabel());
        aItmOpt = new ItemOption("01", "Januari");
        mBlnOption.add(aItmOpt);
        mBlnStringList.add(aItmOpt.getOptLabel());
        aItmOpt = new ItemOption("02", "Februari");
        mBlnOption.add(aItmOpt);
        mBlnStringList.add(aItmOpt.getOptLabel());
        aItmOpt = new ItemOption("03", "Maret");
        mBlnOption.add(aItmOpt);
        mBlnStringList.add(aItmOpt.getOptLabel());
        aItmOpt = new ItemOption("04", "April");
        mBlnOption.add(aItmOpt);
        mBlnStringList.add(aItmOpt.getOptLabel());
        aItmOpt = new ItemOption("05", "Mei");
        mBlnOption.add(aItmOpt);
        mBlnStringList.add(aItmOpt.getOptLabel());
        aItmOpt = new ItemOption("06", "Juni");
        mBlnOption.add(aItmOpt);
        mBlnStringList.add(aItmOpt.getOptLabel());
        aItmOpt = new ItemOption("07", "Juli");
        mBlnOption.add(aItmOpt);
        mBlnStringList.add(aItmOpt.getOptLabel());
        aItmOpt = new ItemOption("08", "Agustus");
        mBlnOption.add(aItmOpt);
        mBlnStringList.add(aItmOpt.getOptLabel());
        aItmOpt = new ItemOption("09", "September");
        mBlnOption.add(aItmOpt);
        mBlnStringList.add(aItmOpt.getOptLabel());
        aItmOpt = new ItemOption("10", "Oktober");
        mBlnOption.add(aItmOpt);
        mBlnStringList.add(aItmOpt.getOptLabel());
        aItmOpt = new ItemOption("11", "November");
        mBlnOption.add(aItmOpt);
        mBlnStringList.add(aItmOpt.getOptLabel());
        aItmOpt = new ItemOption("12", "Desember");
        mBlnOption.add(aItmOpt);
        mBlnStringList.add(aItmOpt.getOptLabel());

        spnBln.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, mBlnStringList));
        spnBln.setSelection(Config.getIndex(mBlnOption, spnBln, mSelectedBln));
        spnBln.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String aItem = spnBln.getSelectedItem().toString();
                String id = mBlnOption.get(i).getOptId();
                String name = mBlnOption.get(i).getOptLabel();
                mSelectedBln = id;
                if (mDebugMode) Toast.makeText(getActivity(), "id : " + id + " - name : " + name, Toast.LENGTH_SHORT).show();

                getRekeningData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        tvRKSaldoAwal = (TextView) view.findViewById(R.id.tvRKSaldoAwal);
        tvRKSaldoAkhir = (TextView) view.findViewById(R.id.tvRKSaldoAkhir);
        tvRKMutasiDb = (TextView) view.findViewById(R.id.tvRKMutasiDb);
        tvRKMutasiCr = (TextView) view.findViewById(R.id.tvRKMutasiCr);
        divRefresh = (LinearLayout) view.findViewById(R.id.divRefresh);

        pb = (ProgressBar) view.findViewById(R.id.pb);
        pb.setVisibility(View.GONE);

        divContainerMutasi = (LinearLayout) view.findViewById(R.id.divContainerMutasi);

        divRefresh = (LinearLayout) view.findViewById(R.id.divRefresh);
        divRefresh.setOnClickListener(new View.OnClickListener() {
            private void doNothing() {

            }

            @Override
            public void onClick(View view) {
                //Toast.makeText(getActivity(), "refresh", Toast.LENGTH_SHORT).show();
                getRekeningData();
            }
        });

        swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(this);
        swipeRefresh.post(new Runnable() {
            private void doNothing() {

            }

            @Override
            public void run() {
                if(!mSelectedRekening.equalsIgnoreCase("")) {
                    getRekeningData();
                }
                else {
                    getRekening();
                }
            }
        });
    }

    @Override
    public void onRefresh() {
        if(!mSelectedRekening.equalsIgnoreCase("")) {
            getRekeningData();
        }
        else {
            getRekening();
        }
    }

    private void getRekening() {
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            private void doNothing() {

            }

            @Override
            public void onResponse(String response) {
                if(mDebugMode) Toast.makeText(getActivity(), "Response : " + response, Toast.LENGTH_LONG).show();
                Log.d("HBB", response);

                if(!TextUtils.isEmpty(response)) {
                    //-- parse result
                    try {
                        JSONObject jObj = new JSONObject(response);
                        String resultStatus = jObj.getString(Config.RESPONSE_STATUS_FIELD);

                        if (resultStatus.trim().equalsIgnoreCase(Config.RESPONSE_STATUS_VALUE_SUCCESS)) {
                            String message = jObj.optString(Config.RESPONSE_MESSAGE_FIELD);
                            JSONArray jsonPayload = jObj.optJSONArray(Config.RESPONSE_PAYLOAD_FIELD);

                            for(int i=0; i<jsonPayload.length(); i++) {
                                JSONObject aItem = jsonPayload.optJSONObject(i);

                                //spinner
                                ItemOption itemOption = new ItemOption(aItem.optString("nomor"), aItem.optString("nomor"));
                                mRekeningOption.add(itemOption);

                                //populate spinner
                                mRekeningStringList.add(itemOption.getOptLabel());
                            }

                            swipeRefresh.setRefreshing(false);

                            //spinner adapter
                            spnRekening.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, mRekeningStringList));
                            spnRekening.setSelection(Config.getIndex(mRekeningOption, spnRekening, mSelectedRekening));
                            spnRekening.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    String aOpt = spnRekening.getSelectedItem().toString();
                                    String id = mRekeningOption.get(i).getOptId();
                                    String name = mRekeningOption.get(i).getOptLabel();
                                    //Toast.makeText(getActivity(),"id : " + id + " - name : " + name, Toast.LENGTH_SHORT).show();
                                    mSelectedRekening = id;

                                    getRekeningData();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });
                        }
                        else {
                            swipeRefresh.setRefreshing(false);
                            Toast.makeText(getActivity(), jObj.getString(Config.RESPONSE_MESSAGE_FIELD), Toast.LENGTH_LONG).show();
                            JSONObject jsonPayload = jObj.optJSONObject(Config.RESPONSE_PAYLOAD_FIELD);
                            if(jsonPayload != null) {
                                if (jsonPayload.optString(Config.RESPONSE_PAYLOAD_API_ACTION).equalsIgnoreCase(Config.RESPONSE_PAYLOAD_API_ACTION_LOGOUT)) {
                                    Config.forceLogout(getActivity());
                                }
                            }
                        }
                    }
                    catch (JSONException e) {
                        swipeRefresh.setRefreshing(false);
                        e.printStackTrace();
                    }
                    //--
                }
                else {
                    swipeRefresh.setRefreshing(false);
                }
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            private void doNothing() {

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefresh.setRefreshing(false);
                //Toast.makeText(getActivity(), Config.ERROR_NETWORK, Toast.LENGTH_SHORT).show();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    String message = "TimeoutError || NoConnectionError";
                    SpannableStringBuilder biggerText = new SpannableStringBuilder(message);
                    biggerText.setSpan(new RelativeSizeSpan(1.1f), 0, message.length(), 0);
                    Toast.makeText(getActivity(), biggerText, Toast.LENGTH_LONG).show();
                    //Toast.makeText(getActivity(),"TimeoutError || NoConnectionError", Toast.LENGTH_LONG).show();
                }
                else if (error instanceof AuthFailureError) {
                    String message = "AuthFailureError";
                    SpannableStringBuilder biggerText = new SpannableStringBuilder(message);
                    biggerText.setSpan(new RelativeSizeSpan(1.1f), 0, message.length(), 0);
                    Toast.makeText(getActivity(), biggerText, Toast.LENGTH_LONG).show();
                    //Toast.makeText(getActivity(),"AuthFailureError", Toast.LENGTH_LONG).show();
                }
                else if (error instanceof ServerError) {
                    String message = "ServerError";
                    SpannableStringBuilder biggerText = new SpannableStringBuilder(message);
                    biggerText.setSpan(new RelativeSizeSpan(1.1f), 0, message.length(), 0);
                    Toast.makeText(getActivity(), biggerText, Toast.LENGTH_LONG).show();
                    //Toast.makeText(getActivity(),"ServerError", Toast.LENGTH_LONG).show();
                }
                else if (error instanceof NetworkError) {
                    String message = "NetworkError";
                    SpannableStringBuilder biggerText = new SpannableStringBuilder(message);
                    biggerText.setSpan(new RelativeSizeSpan(1.1f), 0, message.length(), 0);
                    Toast.makeText(getActivity(), biggerText, Toast.LENGTH_LONG).show();
                    //Toast.makeText(getActivity(),"NetworkError", Toast.LENGTH_LONG).show();
                }
                else if (error instanceof ParseError) {
                    String message = "ParseError";
                    SpannableStringBuilder biggerText = new SpannableStringBuilder(message);
                    biggerText.setSpan(new RelativeSizeSpan(1.1f), 0, message.length(), 0);
                    Toast.makeText(getActivity(), biggerText, Toast.LENGTH_LONG).show();
                    //Toast.makeText(getActivity(),"ParseError", Toast.LENGTH_LONG).show();
                }
            }
        };

        StringRequest dataRequest = new StringRequest(Request.Method.GET, Config.BASE_URL_API_A + "?act=rekening&tipe=tab&loginToken=" + mLoginToken, responseListener, errorListener) {};
        dataRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        swipeRefresh.setRefreshing(true);

        if(mRequestQueue == null) mRequestQueue = Volley.newRequestQueue(getActivity());
        mRequestQueue.add(dataRequest);
    }

    private void getRekeningData() {
        if(mSelectedRekening.equalsIgnoreCase("")) {
            //Toast.makeText(mMainActivity, "Harap pilih rekening", Toast.LENGTH_SHORT).show();
            return;
        }

        Response.Listener<String> responseListener = new Response.Listener<String>() {
            private void doNothing() {

            }

            @Override
            public void onResponse(String response) {
                if(mDebugMode) Toast.makeText(getActivity(), "Response : " + response, Toast.LENGTH_LONG).show();

                if(!TextUtils.isEmpty(response)) {
                    //-- parse result
                    try {
                        JSONObject jObj = new JSONObject(response);
                        String resultStatus = jObj.getString(Config.RESPONSE_STATUS_FIELD);

                        mList.clear();
                        divContainerMutasi.removeAllViews();
                        tvRKSaldoAwal.setText("Rp. -");
                        tvRKMutasiDb.setText("Rp. -");
                        tvRKMutasiCr.setText("Rp. -");
                        tvRKSaldoAkhir.setText("Rp. -");

                        if (resultStatus.trim().equalsIgnoreCase(Config.RESPONSE_STATUS_VALUE_SUCCESS)) {
                            String message = jObj.optString(Config.RESPONSE_MESSAGE_FIELD);
                            JSONObject jsonPayload = jObj.optJSONObject(Config.RESPONSE_PAYLOAD_FIELD);

                            tvRKSaldoAwal.setText("Rp. " + jsonPayload.optString("SALDO_AWAL"));
                            tvRKMutasiDb.setText("Rp. " + jsonPayload.optString("MUTASI_D"));
                            tvRKMutasiCr.setText("Rp. " + jsonPayload.optString("MUTASI_K"));
                            tvRKSaldoAkhir.setText("Rp. " + jsonPayload.optString("SALDO_AKHIR"));

                            JSONArray jsonArray = jsonPayload.optJSONArray("MUTASI");
                            for(int i=0; i<jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                MutasiTabModel item = new MutasiTabModel();
                                item.setId(jsonObject.optInt("Id"));
                                item.setNomor(jsonObject.optString("nomor"));
                                item.setTanggal(jsonObject.optString("tanggal"));
                                item.setTanggalDisplay(jsonObject.optString("tanggalDisplay"));
                                item.setBukti(jsonObject.optString("bukti"));
                                item.setKode(jsonObject.optString("kode"));
                                item.setDk(jsonObject.optString("dk"));
                                item.setMutasiDisplay(jsonObject.optString("mutasiDisplay"));
                                item.setMutasi(jsonObject.optDouble("mutasi"));
                                item.setValidasi(jsonObject.optString("validasi"));
                                item.setUraian(jsonObject.optString("uraian"));

                                mList.add(item);
                            }

                            divContainerMutasi.removeAllViews();
                            for(int i = 0; i< mList.size(); i++) {
                                MutasiTabModel item = mList.get(i);
                                LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                int resId = R.layout.list_row_mutasi_tab_01;
                                final View rowView = layoutInflater.inflate(resId, null);

                                TextView tvTgl = (TextView) rowView.findViewById(R.id.tvTgl);
                                tvTgl.setText(item.getTanggalDisplay());

                                TextView tvMutasi = (TextView) rowView.findViewById(R.id.tvMutasi);
                                tvMutasi.setText(item.getUraian());

                                TextView tvDbCr = (TextView) rowView.findViewById(R.id.tvDbCr);
                                tvDbCr.setText(item.getDk());

                                TextView tvJumlah = (TextView) rowView.findViewById(R.id.tvJumlah);
                                tvJumlah.setText(item.getMutasiDisplay());

                                divContainerMutasi.addView(rowView);
                            }

                            //utk memberi waktu refresh view
                            Runnable runnable = new Runnable() {
                                private void doNothing() {

                                }

                                @Override
                                public void run() {
                                    swipeRefresh.setRefreshing(false);
                                }
                            };
                            Handler handler = new Handler();
                            handler.postDelayed(runnable, 1000);
                        }
                        else {
                            swipeRefresh.setRefreshing(false);
                            Toast.makeText(getActivity(), jObj.getString(Config.RESPONSE_MESSAGE_FIELD), Toast.LENGTH_LONG).show();
                            JSONObject jsonPayload = jObj.optJSONObject(Config.RESPONSE_PAYLOAD_FIELD);
                            if(jsonPayload != null) {
                                if (jsonPayload.optString(Config.RESPONSE_PAYLOAD_API_ACTION).equalsIgnoreCase(Config.RESPONSE_PAYLOAD_API_ACTION_LOGOUT)) {
                                    Config.forceLogout(getActivity());
                                }
                            }
                        }
                    }
                    catch (JSONException e) {
                        swipeRefresh.setRefreshing(false);
                        e.printStackTrace();
                    }
                    //--
                }
                else {
                    swipeRefresh.setRefreshing(false);
                }
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            private void doNothing() {

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefresh.setRefreshing(false);
                Toast.makeText(getActivity(), Config.ERROR_NETWORK, Toast.LENGTH_SHORT).show();
            }
        };

        final Calendar c = Calendar.getInstance();
        int thn = c.get(Calendar.YEAR);
        String bln = mSelectedBln;
        if(mSelectedBln.equalsIgnoreCase("-12")) {
            thn = thn - 1;
            bln = "12";
        }

        String url = Config.BASE_URL_API_A + "?act=tab_mutasi&bln=" + bln + "&thn=" + thn + "&nomor=" + mSelectedRekening + "&loginToken=" + mLoginToken;
        Log.d("HBB", "TabMutasiFragment : " + url);
        StringRequest dataRequest = new StringRequest(Request.Method.GET, url, responseListener, errorListener) {};
        dataRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        swipeRefresh.setRefreshing(true);

        if(mRequestQueue == null) mRequestQueue = Volley.newRequestQueue(getActivity());
        mRequestQueue.add(dataRequest);
    }
}
